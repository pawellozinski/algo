package ploz.algo;

import java.util.List;

public class Utils {

	public static <T> void swap(List<T> l, int i, int j) {
		T tmp = l.get(i);
		l.set(i, l.get(j));
		l.set(j, tmp);
	}

	public static <T> void swap(T[] l, int i, int j) {
		T tmp = l[i];
		l[i] = l[j];
		l[j] = tmp;
	}
	
}
