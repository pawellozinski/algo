package ploz.algo.fn;

public interface Function2<T1, T2, R> {

	public R apply(T1 o1, T2 o2);
	
}
