package ploz.algo.fn;

public interface Function3<T1, T2, T3, R> {

	public R apply(T1 o1, T2 o2, T3 o3);
	
}
