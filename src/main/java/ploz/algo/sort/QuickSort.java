package ploz.algo.sort;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class QuickSort {

	int[] quicksort(int[] A) {
		quicksort(A, 0, A.length - 1);
		return A;
	}

	void quicksort(int[] A, int lo, int hi) {
		if (lo < hi) {
			int splitPoint = partition(A, lo, hi);
			quicksort(A, lo, splitPoint - 1);
			quicksort(A, splitPoint + 1, hi);
		}
	}

	int partition(int[] A, int lo, int hi) {
		int pivot = choosePivot(A, lo, hi);
		swap(A, pivot, hi);
		pivot = lo;
		for (int i = lo; i < hi; i++) {
			if (A[i] < A[hi]) {
				swap(A, pivot, i);
				pivot++;
			}
		}
		swap(A, pivot, hi);
		return pivot;
	}

	void swap(int[] A, int idx1, int idx2) {
		int tmp = A[idx1];
		A[idx1] = A[idx2];
		A[idx2] = tmp;
	}

	int choosePivot(int[] A, int lo, int hi) {
		int half = hi-lo;
		if (A[lo] < A[half]) {
			if (A[lo] < A[hi]) {
				if (A[half] < A[hi]) {
					return half;
				} else {
					return hi;
				}
			} else {
				return lo;
			}
		} else {
			if (A[lo] < A[hi]) {
				return lo;
			} else {
				if (A[half] < A[hi]) {
					return hi;
				} else {
					return half;
				}
			}
		}
	}

	public static void main(String[] args) {
		Random r = new Random();
		int[] a = new int[10000000];
		for(int i=0; i<a.length; i++) {
			a[i] = i;//r.nextInt();
		}
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		new QuickSort().quicksort(a);
		stopwatch.stop();
		
		System.out.println(stopwatch.elapsed(TimeUnit.SECONDS));
	}
	
}
