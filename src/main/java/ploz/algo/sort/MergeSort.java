package ploz.algo.sort;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class MergeSort {

	int[] sort(int[] A) {
		return sort(A, 0, A.length - 1);
	}

	int[] sort(int[] A, int lo, int hi) {
		if (lo == hi)
			return new int[]{A[lo]};
		int split = lo + ((hi-lo) / 2);
		int[] B = sort(A, lo, split);
		int[] C = sort(A, split + 1, hi);
		return merge(B, C);
	}

	int[] merge(int[] B, int[] C) {
		int[] A = new int[B.length + C.length];
		int a = 0, b = 0, c = 0;

		while (b < B.length && c < C.length) {
			if (B[b] < C[c]) {
				A[a++] = B[b++];
			} else {
				A[a++] = C[c++];
			}
		}

		int[] R = b < B.length ? B : C;
		int r = b < B.length ? b : c;

		while (r < R.length) {
			A[a++] = R[r++];
		}

		return A;
	}

	public static void main(String[] args) {
		int[] a = new int[] { 3, 5, 2, 7, 5, 6, 6, 4, 8, 1, 4, 3, 6, 5, 7, 4, 6, 5, 0, 1 };
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(new MergeSort().sort(a)));
		
		Random r = new Random();
		a = new int[10000000];
		for(int i=0; i<a.length; i++) {
			a[i] = a.length-i;//r.nextInt();
		}
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		new MergeSort().sort(a);
		stopwatch.stop();
		
		System.out.println(stopwatch.elapsed(TimeUnit.SECONDS));
		
	}

}
