package ploz.algo.struct;

import java.util.Scanner;

/**
 * 6.2.2. Drzewa licznikowe
 */
public class CountTree {

	private int[] el;
	private int s;

	public CountTree(int size) {
		el = new int[2 * (s = 1 << size)];
		// jvm inits to 0
	}

	public void set(int p, int v) {
		for (p += s; p > 0; p >>= 1)
			el[p] += v;
	}

	public int find(int p, int k) {
		int m = 0;
		p += s;
		k += s;

		while (p < k) {
			if ((p & 1) > 0)
				m += el[p++];
			if (!((k & 1) > 0))
				m += el[k--];
			p >>= 1;
			k >>= 1;
		}

		if (p == k)
			m += el[p];
		return m;
	}

	public static void main(String[] args) {
		int w1, w2, w3;
		CountTree tree = new CountTree(4);

		try (Scanner in = new Scanner(System.in);) {
			do {
				w1 = in.nextInt();
				w2 = in.nextInt();
				w3 = in.nextInt();

				if (w1 == 0) {
					tree.set(w2, w3);
					System.out.println(String.format(
							"Zmiana wartości elementu %d o %d", w2, w3));
				} else {
					System.out.println(String.format(
							"Suma na przedziale [%d..%d] = %d", w2, w3,
							tree.find(w2, w3)));
				}
			} while (w1 != 2);
		}
	}
}
