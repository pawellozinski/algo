package ploz.algo.google;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.google.common.collect.Lists;

class Pair {
	String p, ch;
	public Pair(String p, String ch) {
		this.p = p;
		this.ch = ch;
	}
}

public class ZurichPhone {

	public static Pair p(String p, String ch) {
		return new Pair(p, ch);
	}
	
	public static void main(String[] args) {
		Iterable<Pair> source = Lists.newArrayList(p("c", "cc"), p("a", "c"), p("a", "b"), p("bb", "d"), p("d", "g"), p("b", "e"), p("b", "f"));

		Map<String, List<String>> graph = new HashMap<>();
		Set<String> nonRoots = new HashSet<>();

		// construct graph
		for (Pair p : source) {
			List<String> chs = graph.get(p.p);
			if (chs == null) {
				chs = new ArrayList<String>();
				graph.put(p.p, chs);
			}
			chs.add(p.ch);
			nonRoots.add(p.ch);
		}

		// detect roots
		List<String> roots = new ArrayList<>();
		for (String parent : graph.keySet()) {
			if (!nonRoots.contains(parent))
				roots.add(parent);
		}

		// print
		Stack<String> vertices = new Stack<String>();
		Stack<Integer> depths = new Stack<Integer>();

		for (String r : Lists.reverse(roots)) {
			vertices.push(r);
			depths.push(0);
		}

		while (!vertices.isEmpty()) {
			String v = vertices.pop();
			Integer indent = depths.pop();

			for (int i = 0; i < indent; i++)
				System.out.print("\t");
			System.out.println(v);
			
			List<String> chs = graph.get(v);
			if (chs != null) {
				for (String ch : Lists.reverse(chs)) {
					vertices.push(ch);
					depths.push(indent + 1);
				}
			}
		}

	}
}
