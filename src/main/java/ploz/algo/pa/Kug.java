package ploz.algo.pa;

import java.util.ArrayList;
import java.util.List;

import ploz.algo.fn.Procedure3;
import ploz.algo.kombinatoryka.NumPart;
import ploz.algo.kombinatoryka.PermMinTrAdj;

/**
 * http://main.edu.pl/en/archive/pa/2014/kug
 */
public class Kug {

	public static int solve(int n, List<List<Integer>> C) {

		Procedure3<List<Integer>, List<Integer>, Integer> solve = new Procedure3<List<Integer>, List<Integer>, Integer>() {

			@Override
			public Void apply(List<Integer> s, List<Integer> r, Integer n) {
				List<Integer> division = new ArrayList<>();
				for (int x = 0; x < n; x++) {
					for (int y = 0; y < s.get(x); y++) {
						division.add(r.get(x));
					}
				}
				PermMinTrAdj.f1(division, PermMinTrAdj.PRINTI);
				System.out.println("-------------------------");
				return null;
			}

		};
		
		NumPart.f(n, solve);

		return 0;
	}
	
	public static void main(String[] args) {
		solve(5, null);
	}

}
