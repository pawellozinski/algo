package ploz.algo.kombinatoryka;

import java.util.ArrayList;
import java.util.List;

import ploz.algo.Utils;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

/**
 * 4.2. Permutacje — minimalna liczba transpozycji
 */
public class PermMinTr<T> {

	private final List<T> l;
	private final Predicate<List<T>> f;
	
	public PermMinTr(List<T> l, Predicate<List<T>> f) {
		this.l = new ArrayList<>(l);
		this.f = f;
	}
	
	private void perm(int m) {
		if (m==1) {
			f.apply(l);
		} else {
			for(int i=0; i<m; i++) {
				perm(m-1);
				if (i < m-1) {
					int idx1 = ((m & 1)==0 && m > 2) ? (i < m - 1) ? i : m - 3 : m - 2;
					Utils.swap(l, idx1, m-1);
				}
			}
		}
	}
	
	public void gen() {
		perm(l.size());
	}
	
	public static void main(String[] args) {
	
		Predicate<List<Integer>> f = new Predicate<List<Integer>>() {
			public boolean apply(List<Integer> l) {
				System.out.println(l);
				return true;
			}
		};
		List<Integer> l = Lists.newArrayList(1, 2, 1);
		
		new PermMinTr<>(l, f).gen();
	}
	
}
