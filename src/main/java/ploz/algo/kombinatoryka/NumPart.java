package ploz.algo.kombinatoryka;

import java.util.Arrays;
import java.util.List;

import ploz.algo.fn.Procedure3;

/**
 * 4.7. Podziały liczby w kolejności antyleksykograficznej
 */
public class NumPart {

	public static void f(int n, Procedure3<List<Integer>, List<Integer>, Integer> f) {
		List<Integer> S = Arrays.asList(new Integer[n+1]);
		List<Integer> R = Arrays.asList(new Integer[n+1]);
		int d, sum, l;
		S.set(0, n);
		R.set(0, d = 1);

		while (true) {
			f.apply(R, S, d);
			if (S.get(0) == 1)
				break;
			sum = 0;
			if (S.get(d - 1) == 1)
				sum += R.get(--d);
			sum += S.get(d - 1);
			R.set(d - 1, R.get(d - 1) - 1);
			l = S.get(d - 1) - 1;
			if (R.get(d - 1) > 0)
				d++;
			S.set(d - 1, l);
			R.set(d - 1, sum / l);
			l = sum % l;
			if (l > 0) {
				S.set(d, l);
				R.set(d++, 1);
			}
		}
	}

	public static Procedure3<List<Integer>, List<Integer>, Integer> PRINT = new Procedure3<List<Integer>, List<Integer>, Integer>() {

		@Override
		public Void apply(List<Integer> s, List<Integer> r, Integer n) {
			boolean ch = false;
			for (int x = 0; x < n; x++) {
				for (int y = 0; y < s.get(x); y++) {
					if (ch) System.out.print(" + ");
					System.out.print(r.get(x));
					ch = true;
				}
			}
			System.out.println();
			return null;
		}

	};
	
	
	public static void main(String[] args) {
		f(5, PRINT);
	}

}
