package ploz.algo.kombinatoryka;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;

import ploz.algo.Utils;
import ploz.algo.fn.Procedure2;

public class PermMinTrAdj {

	public static <T> void f(List<T> p, Procedure2<List<T>, Integer> fun) {
		int x, k, i = 0, s = p.size();
		int[] c = new int[s];
		Arrays.fill(c, 1);
		boolean[] pr = new boolean[s];
		Arrays.fill(pr, true);

		c[s - 1] = 0;
		fun.apply(p, -1);
		while (i < s - 1) {
			i = x = 0;
			while (c[i] == s - i) {
				if (pr[i] = !pr[i])
					x++;
				c[i++] = 1;
			}
			if (i < s - 1) {
				k = pr[i] ? c[i] + x : s - i - c[i] + x;
				Utils.swap(p, k - 1, k);
				fun.apply(p, k - 1);
				c[i]++;
			}
		}
	}

	public static <T> void f1(List<T> p, Procedure2<List<T>, Integer> fun) {
		int x, k, i = 0, s = p.size();
		int[] c = new int[s];
		Arrays.fill(c, 1);
		boolean[] pr = new boolean[s];
		Arrays.fill(pr, true);

		c[s - 1] = 0;
		fun.apply(p, -1);
		while (i < s - 1) {
			i = x = 0;
			while (c[i] == s - i) {
				if (pr[i] = !pr[i])
					x++;
				c[i++] = 1;
			}
			if (i < s - 1) {
				k = pr[i] ? c[i] + x : s - i - c[i] + x;
				if (!p.get(k).equals(p.get(k - 1))) {
					Utils.swap(p, k - 1, k);
					fun.apply(p, k - 1);
				}
				c[i]++;
			}
		}
	}

	public static final Procedure2<List<Integer>, Integer> PRINTI = new Procedure2<List<Integer>, Integer>() {
		@Override
		public Void apply(List<Integer> o1, Integer o2) {
			System.out.println(o1);
			return null;
		}
	};

	public static void main(String[] args) {
		List<Integer> l = Lists.newArrayList(1, 1, 2);
		f1(l, PRINTI);
	}

}
